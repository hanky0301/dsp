#! /usr/bin/env python

import sys
from collections import defaultdict


def build_map(map_file, new_map_file):
    zhuyin_big5_map = defaultdict(set)

    with open(map_file, 'r') as f:
        for line in f:
            split = line.split()
            character = split[0]
            chuyin_list = split[1].split('/')

            zhuyin_big5_map[character].add(character)
            for chuyin in chuyin_list:
                zhuyin_big5_map[chuyin[:2]].add(character)

    with open(new_map_file, 'w') as f:
        for key, value in zhuyin_big5_map.iteritems():
            f.write('{} {}\n'.format(key, ' '.join(value)))


if __name__ == "__main__":
    build_map(sys.argv[1], sys.argv[2])

