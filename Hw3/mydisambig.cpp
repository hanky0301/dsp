#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cfloat>

#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include <algorithm>
#include "Ngram.h"

using namespace std;


typedef unordered_map< string, vector<string> >		MAP_TYPE;
typedef vector< vector<string> >					INPUT_TYPE;

void build_map(char map_filename[], MAP_TYPE& map);

void build_input_vector(char input_filename[], INPUT_TYPE& input_vec);

void decode(INPUT_TYPE& input_vec, MAP_TYPE& map, Ngram &lm, Vocab &voc);

double getBigramProb(const char *w1, const char *w2, Ngram &lm, Vocab &voc);

int main(int argc, char* argv[])
{
	MAP_TYPE map;
	build_map(argv[2], map);
    Vocab voc;
    Ngram lm(voc, 2);
	
	File lmFile(argv[3], "r");
	lm.read(lmFile);
	lmFile.close();
	
	INPUT_TYPE input_vec;
	build_input_vector(argv[1], input_vec);
	decode(input_vec, map, lm, voc);

	return 0;
}

void build_map(char map_filename[], MAP_TYPE& map)
{
	FILE* fp = fopen(map_filename, "r");
	char line[10000];
	while (fgets(line, 10000, fp) != NULL)
	{
		line[strlen(line) - 1] = '\0';
		char* pch = strtok(line, " ");
		string key = string(pch);
		pch = strtok(NULL, " ");
		while (pch != NULL)
		{
			map[key].push_back(string(pch));
			pch = strtok(NULL, " ");
		}
	}
	map["<s>"].push_back("<s>");
	map["</s>"].push_back("</s>");

	fclose(fp);
	return;
}

void build_input_vector(char input_filename[], INPUT_TYPE& input_vec)
{
	FILE* fp = fopen(input_filename, "r");
	char line[1000];
	while (fgets(line, 1000, fp) != NULL)
	{
		input_vec.push_back(vector<string>());
		input_vec.back().push_back("<s>");

		line[strlen(line) - 1] = '\0';
		char* pch = strtok(line, " ");
		while (pch != NULL)
		{
			input_vec.back().push_back(string(pch));
			pch = strtok(NULL, " ");
		}
		input_vec.back().push_back("</s>");
	}

	return;
}

// Get P(W2 | W1) -- bigram
double getBigramProb(const char *w1, const char *w2, Ngram &lm, Vocab &voc)
{
    VocabIndex wid1 = voc.getIndex(w1);
    VocabIndex wid2 = voc.getIndex(w2);

    if (wid1 == Vocab_None)  //OOV
        wid1 = voc.getIndex(Vocab_Unknown);
    if (wid2 == Vocab_None)  //OOV
        wid2 = voc.getIndex(Vocab_Unknown);

    VocabIndex context[] = {wid1, Vocab_None};
    return lm.wordProb(wid2, context);
}

void decode(INPUT_TYPE& input_vec, MAP_TYPE& map, Ngram &lm, Vocab &voc)
{
	for (int i = 0; i < input_vec.size(); i++)
	{
		vector<string> seq = input_vec[i];
		double delta[seq.size()][1500];
		int parent[seq.size()][1500];
		memset(delta, 0, sizeof(delta));
		
		for (int j = 1; j < seq.size(); j++)
		{
			for (int k = 0; k < map[seq[j]].size(); k++)
			{
				delta[j][k] = -DBL_MAX;
				for (int t = 0; t < map[seq[j - 1]].size(); t++)
				{
					double trans_p = getBigramProb(map[seq[j - 1]][t].c_str(), 
												   map[seq[j]][k].c_str(), lm, voc);
					if (delta[j - 1][t] + trans_p > delta[j][k])
					{
						delta[j][k] = delta[j - 1][t] + trans_p;
						parent[j][k] = t;
					}
				}
			}
		}
		vector<string> output(seq.size());
		output[output.size() - 1] = "</s>";
		int child = 0;
		for (int j = output.size() - 2; j >= 0; j--)
		{
			output[j] = map[seq[j]][parent[j + 1][child]];
			child = parent[j + 1][child];
		}
		for (int j = 0; j < output.size() - 1; j++)
			cout << output[j] << " ";
		cout << output[output.size() - 1] << "\n";
	}

	return;
}
