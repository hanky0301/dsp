#-*- coding: big5 -*-
import sys
f = open(sys.argv[1], "rt")
map = {}
for line in f:
    char = line[:line.find(" ")]
    zu_list = line[line.find(" ") + 1:].strip().split("/")
    for zu in zu_list:
        key = zu[:2]
        if key not in map:
            map[key] = set()
            map[key].add(char)
        else:
            map[key].add(char)

out = open(sys.argv[2], "wt")
for key in map:
    out.write(key + " ")
    for char in map[key]:
        out.write(char + " ")
    out.write("\n")
    for char in map[key]:
        out.write(char + " " + char)
        out.write("\n")

#print max([len(map[key]) for key in map])
