#include <stdio.h>
#include <string>
#include <map>
#include <vector>
#include "Ngram.h"

using namespace std;

void init_map(map<string, vector<string> > &Map, char *filename)
{
    FILE *fp;
    char line[10000];
    char* tok;
    string key;
    int num_char;
    int i;
    fp = fopen(filename, "r");
    while (fgets(line, 10000, fp) != NULL) {
        if (line[strlen(line) - 1] == '\n')
            line[strlen(line) - 1] = '\0';
        tok = strtok(line, " ");
        key = tok;
        tok = strtok(NULL, " ");
        while (tok != NULL) {
            Map[key].push_back(tok);
            tok = strtok(NULL, " ");
        }
    }
    Map["<s>"].push_back("<s>");
    Map["</s>"].push_back("</s>");
    return;
}

void init_char_vec(vector<string> &v, char line[])
{
    char* tok;
    tok = strtok(line, " ");
    v.push_back("<s>");
    while (tok != NULL) {
        v.push_back(tok);
        tok = strtok(NULL, " ");
    }
    v.push_back("</s>");
    return;
}

// Get P(W2 | W1) -- bigram
double getBigramProb(const char *w1, const char *w2, Ngram &lm, Vocab &voc)
{
    VocabIndex wid1 = voc.getIndex(w1);
    VocabIndex wid2 = voc.getIndex(w2);

    if(wid1 == Vocab_None)  //OOV
        wid1 = voc.getIndex(Vocab_Unknown);
    if(wid2 == Vocab_None)  //OOV
        wid2 = voc.getIndex(Vocab_Unknown);

    VocabIndex context[] = { wid1, Vocab_None };
    return lm.wordProb( wid2, context);
}

string viterbi(vector<string> &v, map<string, vector<string> >Map, Ngram &lm, Vocab &voc)
{
    int i, j, k;
    double table[v.size()][1500] = {0};//max disambig
    double max, prob;
    int prev[v.size()][1500] = {0};//max disambig
    int next;
    //viterbi
    for (i = 1; i < v.size(); i++) {
        for (j = 0; j < Map[v[i]].size(); j++) {
            //k == 0
            prob = table[i - 1][0] + getBigramProb(Map[v[i - 1]][0].c_str(), Map[v[i]][j].c_str(), lm, voc);
            max = prob;
            prev[i][j] = 0;
            table[i][j] = prob;
            //k->j
            for (k = 1; k < Map[v[i - 1]].size(); k++) {
                if ((prob = table[i - 1][k] + getBigramProb(Map[v[i - 1]][k].c_str(), Map[v[i]][j].c_str(), lm, voc)) > max) {
                    table[i][j] = prob;
                    max = prob;
                    prev[i][j] = k;
                }
            }
        }
    }
    vector<string> ans_vec(v.size());
    ans_vec[v.size() - 1] = "</s>";
    next = 0;
    //backtrace
    for (i = v.size() - 1; i > 0; i--) {
        ans_vec[i - 1] = Map[v[i - 1]][prev[i][next]];
        next = prev[i][next];
    }
    string ans = "";
    for (i = 0; i < v.size() - 1; i++) {
        ans += ans_vec[i];
        ans += " ";
    }
    ans += ans_vec[v.size() - 1];
    ans += "\n";
    return ans;
}

//testdata, lm, map
int main(int argc, char* argv[])
{
    int ngram_order = 2;
    int i;
    char line[5000];
    Vocab voc;
    Ngram lm(voc, ngram_order);
    map<string, vector<string> > zhuyin_map;
    {
        //load lm
        char* lm_filename = argv[2];
        File lmFile(lm_filename, "r");
        lm.read(lmFile);
        lmFile.close();
        //init map
        char* map_filename = argv[3];
        init_map(zhuyin_map, map_filename);
    }
    char* data_filename = argv[1];
    FILE *fp = fopen(data_filename, "r");
    while (fgets(line, 5000, fp) != NULL) {
        if (line[strlen(line) - 1] == '\n')
            line[strlen(line) - 1] = '\0';
        vector<string> char_vec;
        init_char_vec(char_vec, line);
        //disambig
        string dis;
        dis = viterbi(char_vec, zhuyin_map, lm, voc);
        printf("%s", dis.c_str());
    }
    return 0;
}
