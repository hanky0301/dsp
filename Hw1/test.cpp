#include "hmm.h"

#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <string>
#include <fstream>

#define N			2500
#define T			50
#define STATE_NUM	6

using namespace std;


int main(int argc, char* argv[])
{
	HMM hmm[5];
	double delta[T][STATE_NUM] = {0};

	string seq[N];
	ifstream seq_file(argv[2]);
	for (int n = 0; n < N; n++)
		getline(seq_file, seq[n]);
	FILE* fp = fopen(argv[3], "w");

	load_models(argv[1], hmm, 5);

	for (int n = 0; n < N; n++)
	{
		int max_model;
		double max_prob = 0;
		
		for (int m = 0; m < 5; m++)
		{
			for (int i = 0; i < STATE_NUM; i++)
			{
				int observ = seq[n][0] - 'A';
				delta[0][i] = hmm[m].initial[i] * hmm[m].observation[observ][i];
			}
			for (int t = 1; t < T; t++)
			{
				for (int j = 0; j < STATE_NUM; j++)
				{
					double max = 0;
					for (int i = 0; i < STATE_NUM; i++)
						if (delta[t - 1][i] * hmm[m].transition[i][j] > max)
							max = delta[t - 1][i] * hmm[m].transition[i][j];
					int observ = seq[n][t] - 'A';

					delta[t][j] = max * hmm[m].observation[observ][j];
				}
			}
			double P = 0;
			for (int i = 0; i < STATE_NUM; i++)
			{
				if (delta[T - 1][i] > P)
					P = delta[T - 1][i];
			}

			if (P > max_prob)
			{
				max_prob = P;
				max_model = m;
			}	
		}
		fprintf(fp, "model_0%d.txt %e\n", max_model + 1, max_prob);
	}
	return 0;
}
