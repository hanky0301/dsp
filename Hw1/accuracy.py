ans = []
for line in open('testing_answer.txt'):
    ans.append(line.strip())

res = []
for line in open('result1.txt'):
    res.append(line.strip().split()[0])

cnt = 0
for a, b in zip(ans, res):
    if(a == b):
        cnt += 1
print('accuracy:', cnt/len(ans))
