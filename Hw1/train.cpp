#include "hmm.h"

#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <string>
#include <fstream>

#define STATE_NUM 	6
#define OBSERV_NUM 	6
#define T 	50
#define N  	10000

using namespace std;


int main(int argc, char* argv[])
{
	int max_iter = atoi(argv[1]);

	string seq[N];
	ifstream seq_file(argv[3]);
	for (int n = 0; n < N; n++)
		getline(seq_file, seq[n]);

	HMM hmm;
	loadHMM(&hmm, argv[2]);

	for(int iter = 0; iter < max_iter; iter++)
	{
		int n_sample = 0;
		FILE* fp = open_or_die(argv[3], "r");

		double sigma_gamma[STATE_NUM] = {0};
		double init_gamma[STATE_NUM] = {0};
		double sigma_epsilon[STATE_NUM][STATE_NUM] = {0};
		double sigma_observ_gamma[OBSERV_NUM][STATE_NUM] = {0};

		for (int n = 0; n < N; n++)
		{
			double alpha[T][STATE_NUM] = {0};
			double beta[T][STATE_NUM] = {0};
			double gamma[T][STATE_NUM] = {0};
			double epsilon[T][STATE_NUM][STATE_NUM] = {0};

			n_sample += 1;
			
			/* Compute alpha */
			for (int i = 0; i < hmm.state_num; i++)
			{
				int observ = seq[n][0] - 'A';
				alpha[0][i] = hmm.initial[i] * hmm.observation[observ][i];
			}
			for (int t = 0; t < T - 1; t++)
			{
				for (int j = 0; j < hmm.state_num; j++)
				{
					double sum = 0;
					for (int i = 0; i < hmm.state_num; i++)
						sum += alpha[t][i] * hmm.transition[i][j];
					int observ = seq[n][t + 1] - 'A';
					alpha[t + 1][j] = sum * hmm.observation[observ][j];
				}
			}
			
			/* Compute beta */
			for (int i = 0; i < hmm.state_num; i++)
				beta[T - 1][i] = 1;
			for (int t = T - 2; t >= 0; t--)
			{
				for (int i = 0; i < hmm.state_num; i++)
				{
					beta[t][i] = 0;
					for (int j = 0; j < hmm.state_num; j++)
					{
						int observ = seq[n][t + 1] - 'A';
						beta[t][i] += hmm.transition[i][j] 
									  * hmm.observation[observ][j] 
									  * beta[t + 1][j];
					}
				}
			}
			
			/* Compute gamma */
			for (int t = 0; t < T; t++)
			{
				double sum = 0;
				for (int i = 0; i < hmm.state_num; i++)
					sum += alpha[t][i] * beta[t][i];
				for (int i = 0; i < hmm.state_num; i++)
					gamma[t][i] = alpha[t][i] * beta[t][i] / sum;
			}

			/* Compute epsilon */
			for (int t = 0; t < T; t++)
			{
				double sum = 0;
				for (int i = 0; i < hmm.state_num; i++)
				{
					for (int j = 0; j < hmm.state_num; j++)
					{
						int observ = seq[n][t + 1] - 'A';
						sum += alpha[t][i] 
							   * hmm.transition[i][j] 
							   * hmm.observation[observ][j]
							   * beta[t + 1][j];
					}
				}
				for (int i = 0; i < hmm.state_num; i++)
				{
					for (int j = 0; j < hmm.state_num; j++)
					{
						int observ = seq[n][t + 1] - 'A';
						epsilon[t][i][j] = alpha[t][i] 
										   * hmm.transition[i][j] 
										   * hmm.observation[observ][j] 
										   * beta[t + 1][j] 
										   / sum;
					}
				}
			}

			/* Compute initial of gamma */
			for (int i = 0; i < hmm.state_num; i++)
				init_gamma[i] += gamma[0][i];

			/* Compute sigma of gamma */
			for (int t = 0; t < T - 1; t++)
			{
				for (int i = 0; i < hmm.state_num; i++)
				{
					sigma_gamma[i] += gamma[t][i];
					
					int observ = seq[n][t] - 'A';
					sigma_observ_gamma[observ][i] += gamma[t][i];
				}
			}

			/* Compute sigma of epsilon */
			for (int t = 0; t < T - 1; t++)
				for (int i = 0; i < hmm.state_num; i++)
					for (int j = 0; j < hmm.state_num; j++)
						sigma_epsilon[i][j] += epsilon[t][i][j];

		}
		
		/* Update hmm.initial */
		for(int i = 0; i < hmm.state_num; i++)
			hmm.initial[i] = init_gamma[i] / n_sample;

		/* Update hmm.transition */
		for(int i = 0; i < hmm.state_num; i++)
			for(int j = 0; j < hmm.state_num; j++)
				hmm.transition[i][j] = sigma_epsilon[i][j] / sigma_gamma[i];

		/* Update hmm.observation */
		for(int i = 0; i < hmm.observ_num; i++)
			for(int j = 0; j < hmm.state_num; j++)
				hmm.observation[i][j] = sigma_observ_gamma[i][j] / sigma_gamma[j];

	}

	FILE* fp = fopen(argv[4], "w");
	dumpHMM(fp, &hmm);

	return 0;
}
